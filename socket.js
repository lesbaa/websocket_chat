const WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({ port: 4040 });

module.exports = wss;
