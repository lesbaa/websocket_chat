import WsManager from './modules/wsManager'

window.wsm = new WsManager('localhost', 4040);

wsm.newConnection(null, null, 'one');

const isMe = document.getElementById('is-me').dataset.isMe == 'true';

function randomHash(length) {
  let hex = '0123456789abcdef'.split(''),
      hash = '';

  for (let i = 0; i < length; i++) {
    hash += hex[Math.floor(Math.random() * 16)];
  }

  return hash;
}

function Sqr(x,y,dx,dy) {
  this.x  = x,
  this.y  = y,
  this.dx = dx || 0,
  this.dy = dy || 0,
  this.id = 'sqr' + randomHash(4);
  this.create = ()=> {
    let el = document.createElement('div')
        el.classList.add('wee-square');
    el.id = this.id;

    document.body.appendChild(el);
    this.element = document.querySelector(`#${this.id}`)
  },
  this.move = ()=> {
    this.x += this.dx;
    this.y += this.dy;
    this.updateElPos();
  },

  this.updateElPos = ()=> {
    this.element.style.top  = this.y + 'px';
    this.element.style.left = this.x + 'px';
  },
  this.getData = ()=> {
    return {
      x: this.x,
      y: this.y,
      dy: this.dy,
      dx: this.dx
    }
  };

  this.create(x,y,dx,dy);

}

let sqr = new Sqr(window.innerWidth / 2,window.innerHeight / 2,0,0)

function keyHandler(event) {
  switch (event.key) {
    case 'ArrowUp':
      sqr.dy += -1
    break;
    case 'ArrowDown':
      sqr.dy += 1
    break;
    case 'ArrowLeft':
      sqr.dx += -1
    break;
    case 'ArrowRight':
      sqr.dx += 1
    break;
  }
}

function mouseHandler(event) {
  sqr.dx = ( event.clientX - (window.innerWidth / 2) ) / 50;
  sqr.dy = ( event.clientY - (window.innerHeight / 2) ) / 50;
}

// console.log(ws);

let data  = {},
    frame = 0

function animLoop() {
  if (isMe) {
    wsm.conns['one'].sendJSON( data );
  }
  window.requestAnimationFrame(animLoop);
  sqr.move();
  frame++;
};

wsm.conns['one'].addEventListener('open',()=>{
  if (isMe) {
    window.addEventListener('keydown', keyHandler);
    // window.addEventListener('mousemove', mouseHandler);

  } else {
    wsm.conns['one'].onmessage = (event) =>{
      let data = JSON.parse( event.data )
      Object.keys(data).forEach( (el, ind)=>{
        sqr[el] = data[el];
      })
    }
  }
  animLoop();
})
