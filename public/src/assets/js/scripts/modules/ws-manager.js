export function WsManager(defaultPort) {

  this.conns = {};

  this.newConnection = function(url, port, protocol) {
    let newPort     = defualtPort ? defualtPort : 8080;
    let newProtocol = protocol ? protocol : (()=>{ console.error('no protocol specified : please specify a protocol name'); })();
    let newUrl      = url ? url : (()=>{ console.error('no url specified : please specify a url'); })() );

    this.conns[protocol] = new WebSocket(`ws://${url}:${port}`, protocol);
  }

  this.sendJSON = function(protocol, data) {
    let parsedData = JSON.stringify( data );
    this.conns[protocol].send( parsedData )
  }

  this.listen = function(protocol, handler) {
    this.conns[protocol].addEventListener('message', e =>{
      handler.apply(null, e)
    })
  }

  this.closeConnection = function(protocol) {
    this.conns[protocol].close();
  }

}
