export default function WsManager(defaultUrl, defaultPort) {

  this.defaultPort = defaultPort;
  this.defaultUrl  = defaultUrl;

  this.conns = {};
  this.activePorts = [];

  this.newConnection = function(url, port, protocol) {
    let newPort     = port ? port : this.defaultPort;
    let newProtocol = protocol ? protocol : (()=>{ console.error('no protocol specified : please specify a protocol name'); return null})();
    let newUrl      = !!url ? url : this.defaultUrl;

    if ( this.activePorts.length > 0 && this.activePorts.indexOf( port ) !== -1 ) {
      this.conns[protocol] = new WebSocket(`ws://${url}:${port}`, protocol);
      this.activePorts.push(port);
    } else {
      console.error( port + ': port already in use!');
    }
  }

  this.sendJSON = function(protocol, data) {
    let parsedData = JSON.stringify( data );
    this.conns[protocol].send( parsedData );
  }

  this.listen = function(protocol, handler) {
    this.conns[protocol].onmessage = e => {
      console.log('message received')
      handler.apply(null, e)
    }
  }

  this.closeConnection = function(protocol) {
    let indexOfPort = this.activePorts.indexOf( port );
    this.conns[protocol].close();
    this.activePorts.slice(indexOfPort, indexOfPort + 1);
  }

}
