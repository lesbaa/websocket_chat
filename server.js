const express     = require('express'),
      app         = express(),
      port        = process.env.PORT || 8080,
      wss         = require('./socket');

// serve files from dist folder
app.use('/', express.static( 'public/dist', {index: "index.html"}) )
app.listen(port, ()=> {
  console.log('listening on port ' + port);
});

wss.on('connection', function connection(ws) {
  ws.on('message', function(message) {
    wss.clients.forEach(function each(client) {
      if (client !== ws) client.send(message);
      console.log(client.protocol);
    });
  });
  ws.send('so it seems to be working');
});
