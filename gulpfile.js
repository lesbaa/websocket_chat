var fs      = require('fs'),
gulp        = require('gulp'),
sass        = require('gulp-ruby-sass'),
minifycss   = require('gulp-minify-css'),
jshint      = require('gulp-jshint'),
uglify      = require('gulp-uglify'),
imagemin    = require('gulp-imagemin'),
rename      = require('gulp-rename'),
concat      = require('gulp-concat'),
notify      = require('gulp-notify'),
logger      = require('gulp-logger'),
cache       = require('gulp-cache'),
babelify    = require('babelify');
gulpIgnore  = require('gulp-ignore'),
bro         = require('gulp-bro'),
del         = require('del'),
sourcemaps  = require('gulp-sourcemaps'),
inject      = require('gulp-inject'),
plumber     = require('gulp-plumber'),
browserify  = require('gulp-browserify'),
runSequence = require('gulp-run-sequence');
// minify html and move

gulp.task('html', function(){
  return gulp.src(['./public/src/*.html','./public/src/*.php'])

  .pipe(gulp.dest('./public/dist'));
});

// compile sass and minify

gulp.task('styles', function(){
  return sass('./public/src/assets/styles/**/*.sass')
  .pipe(rename({suffix: '.min'}))
  .pipe(
      logger({
        before: 'compiling sass...',
        after: 'styles task complete',
        display: 'name',
        showChange: true
      })
  )
  .pipe(minifycss({keepSpecialComments: 0}))
  .pipe(gulp.dest('./public/dist/assets/styles'))
});

gulp.task('bundle-dev', function() {
  return gulp.src('./public/src/assets/js/scripts/main.js',{ read: false })
    .pipe(bro({
      transform: [
        babelify.configure({ presets: ['es2015'] })
      ]
    }))
    .pipe( gulp.dest('./public/dist/assets/js/scripts') )
})

gulp.task('bundle', function() {
  return gulp.src('./public/src/assets/js/scripts/main.js',{ read: false })
    .pipe(bro({
      transform: [
        babelify.configure({ presets: ['es2015'] })
      ]
    }))
    .pipe( uglify() )
    .pipe( rename({suffix: '.min'}) )
    .pipe( gulp.dest('./public/dist/assets/js/scripts') )
})

// optimise images

gulp.task('images', function() {
  return gulp.src('./public/src/assets/img/**/**/**')
  .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
  .pipe(rename(function(path) {
      path.basename = path.basename.replace('_', '')
  }))
  .pipe(
    logger({
      before: 'optimising images...',
      after: 'images optimised and moved',
      display: 'name',
      showChange: true
    })
  )
  .pipe(gulp.dest('./public/dist/assets/img'));
});

// gulp inject task

gulp.task('inject', function () {
  return gulp.src(['public/dist/index.html','public/dist/not-index.html'])
    .pipe( inject( gulp.src('public/dist/assets/js/libs/*.js', {read: false}), { starttag: '<!-- inject:libs -->', relative: true, removeTags: true } ) )
    .pipe( inject( gulp.src(['public/dist/assets/js/scripts/*.js', 'public/dist/**/*.css'], {read: false}), { relative: true, removeTags: true } )  )
    .pipe(gulp.dest('public/dist'));
})

// Font task

gulp.task('fonts', function () {
    return gulp.src('./public/src/assets/fonts/**')
        .pipe(gulp.dest('./public/dist/assets/fonts'));
});

// clean up task

gulp.task('clean', function() {
  return del(['./public/dist']);
});

// Watch
gulp.task('dev-watch', function() {

  // Watch html files
  gulp.watch('./public/src/*.html', function() { runSequence('html','inject') } );

  // Watch .sass files
  gulp.watch(['./public/src/assets/styles/*.sass','./public/src/assets/sass/**/*','bower_components/**/*'], ['styles']);

  // Watch .js files
  gulp.watch('./public/src/assets/js/scripts/**/*.js', function() { runSequence('bundle-dev','inject') } );


  // Watch image files
  gulp.watch('./public/src/assets/img/**', ['images']);

  // Watch fonts files
  gulp.watch('./public/src/assets/fonts/**', ['fonts']);

});

// set up default task
gulp.task('dev',['clean'], function(){
    runSequence(['bundle-dev','styles','html'],['images','fonts','inject'],'dev-watch');
});

gulp.task('build',['clean'], function(){
    runSequence(['bundle','styles','html'],['images','fonts','inject']);
});
